#!/bin/bash
#steps to do
#Reboot your Android device in Recovery Mode
#Create backup user data from Recovery Mode (an SD card is required to create a file similar with: userdata_xxxx_xxxxxx.backup).
#Copy the userdata_xxxx_xxxxxx.backup from SD card to Desktop.

red="\e[0;31m"
green="\e[0;32m"
off="\e[0m"

function banner() {
clear
printf "\033[1;31m                                          _              _       _            \e[0m\n";

printf "\033[1;31m                                         / \   _ __   __| |_ __ / \   ___ ___ \e[0m\n";

printf "\033[1;31m                                        / _ \ | '_ \ / _  | '__/ _ \ / __/ _ \   \e[0m\n";

printf "\033[1;31m                                       / ___ \| | | | (_| | | / ___ \ (_|  __/  \e[0m\n";

printf "\033[1;31m                                      /_/   \_\_| |_|\__,_|_|/_/   \_\___\___| \e[0m\n";

printf "                                                                                                    ";

printf "\e[0;49;97m                                                                          [Version 1.1.0]                    \e[0m\n";

printf "\e[0;32m                                                     Presented By:-                    \e[0m\n";

printf "\e[5;49;97m\e[45m                                             TAPs - The Ace Programmings                                              \e[0m\n";


printf "\e[1;92m                                                                                    \e[0m\n";


}

function linux() {

clear

 printf

      printf "$red [$green+$red]$off \033[1;49;91mBackup found!!\e[0m\n";

sleep 5

  printf "";

       printf "$red [$green+$red]$off \033[1;49;91mTool is going to start now!!\e[0m\n";
printf
  sleep 5
   
ls

  sleep 5

    printf "";

     printf "$red [$green+$red]$off \033[1;49;91mplease enter the complete name of backup here from above list\e[0m\n";

sleep 3
   
  read backup_name

 cd ~/Desktop/

sleep 3

dd if=$backup_name bs=512 skip=1 of=backup.tar.gz

sleep 3

 mkdir backup

sleep 3

  tar -xvf backup.tar.gz -C ~/Desktop/backup

sleep 3

  python $HOME/TAP4/Requirement/aplc.py ~/Desktop/backup/system/gesture.key



}

sleep 3

   banner

    printf "$red [$green+$red]$off \033[1;49;91mATTENTION: move this (TAP4) file in only home directory\e[0m\n";

sleep 3

echo
   
 printf "$red [$green+$red]$off                        and\e[0m\n";

sleep 3

echo

      printf "$red [$green+$red]$off \033[1;49;91mPut your recovery backup on desktop (E.g:-userdata_xxxx_xxxxxx.backup)\e[0m\n";

sleep 3

  if [ -e ~/Desktop/*.backup ];then

     printf "$red [$green+$red]$off \033[0;49;91mAndrAce is starting. please wait...\e[0m\n";

      linux

    sleep 3

   else

echo

sleep 3
       printf "$red [$green+$red]$off \033[1;49;91mNo backup on desktop, check and try again!\e[0m\n";

echo

sleep 3

printf "$red [$green+$red]$off \033[1;49;91mexiting...\e[0m\n";

echo

sleep 3

clear

  fi
